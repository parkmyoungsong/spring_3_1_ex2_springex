package com.javalec.ex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring31Ex2SpringexApplication {

	public static void main(String[] args) {
		SpringApplication.run(Spring31Ex2SpringexApplication.class, args);
	}
}
